function exercicio18()

    y1 = sqrt(cos(pi/2 - 0.000049));
    y2 = sqrt(0.000049);
        
    fprintf('y1\t\t\ty2\t\t\tErro relativo\n');
    fprintf('-----------------------------------\n');
    fprintf('%.6f\t%.6f\t%.6f%%\n',y1,y2,abs(y1/y2)*100);
    
    fprintf('\n\nUsando s� 6 digitos de precis�o n�o d� para perceber a diferencia\n\n\n');
        
    fprintf('y1\t\t\ty2\t\t\tErro relativo\n');
    fprintf('-----------------------------------\n');
    fprintf('%.15f\t%.15f\t%.15f%%\n',y1,y2,abs(y1/y2)*100);
    
end
