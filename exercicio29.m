
clc;
fprintf('1)\n\n');
secantes((@(x)4*cos(x) - exp(2*x)),0.3,0.6,0.000001);
fprintf('\n\n\n');
fprintf('\n\n\n');

fprintf('2)\n\n');
secantes((@(x)x/2 - tan(x)),-0.1,0.1);
fprintf('\n\n\n');
fprintf('\n\n\n');

fprintf('3)\n\n');
secantes((@(x)1 - (x.*log(x))),1.7,1.8,0.000001);
fprintf('\n\n\n');
fprintf('\n\n\n');

fprintf('4)\n\n');
secantes((@(x)2.^x - 3*x),3.2,3.4,0.000001);
fprintf('\n\n\n');
fprintf('\n\n\n');

fprintf('5)\n\n');
secantes((@(x)x.^3 + x - 1000),9.9,10,0.000001);

function secantes(func,a,b,tol,maxit)
    %Programa criado por Prof Marcos Figueredo
    %data: 01/02/2018
    %entrada  
    %func -> funcao objetivo
    %a -> chute inicial
    %tol -> tolerancia maxima, caso nao seja informado sera tol=0.0001
    %maxit -> maximo de iteracoes, caso nao seja informado sera 50
    %Exemplo:secantes(@(x) x^3-x-2,1,2)

    if nargin<3
        error('Sao necessarios pelo menos 3 argumentos de entrada!'),end
    if nargin<4||isempty(tol),tol=0.0001;end
    if nargin<5||isempty(maxit), maxit=50;end
    i=1; 
    xr=0.01;
    fprintf('\t=====================================\n');
    fprintf('\t\tMetodo da Secante\n');
    fprintf('\t=====================================\n\n');
    fprintf('-----------------------------------------------\n');
    fprintf('%2s%10s%10s%12s%14s%13s\n','N','x(i-1)','xi','x(i+1)','|er|');
    fprintf('\n-----------------------------------------------\n');
    while(abs(func(xr))>tol)
        if maxit==i,break,end
           xr=a-(func(a)*(a-b))/(func(a)-func(b));
           er=abs((xr-a)/xr)*100;
           fprintf('%d\t%6.6f\t%6.6f\t%6.6f\t%8.4f%%\t\n',i,a,b,xr,er);
           b=a;
           a=xr;
           i=i+1;
    end
end
