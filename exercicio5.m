 function exercicio5()
    %A equa��o da Borboleta � dada pelas seguintes equa��es param�tricas:
    %*ver equa�oes no pdf*
    %Gere valores de x e y para t [0, 100] com ?t = 1/16 e construa gr�ficos de:
    %1. x, y, t
    %2. y, x
    %Use o comando subplot para organizar esses gr�ficos verticalmente e fa�a o gr�fico de (b)
    %quadrado. inclua t�tulos e identifica��es dos eixos em ambos os gr�ficos e uma legenda
    %em (a). Para (a) empregue uma linha tracejada para y de modo a distingui-la de x.
    %
    %Entrada:
    %
    
    t=0:1/16:100;
    
    x=sin(t).*(exp(cos(t)) - 2.*cos(4.*t) - (sin(t./12)).^5);
    
    y=cos(t).*(exp(cos(t)) - 2.*cos(4.*t) - (sin(t./12)).^5);
    
    subplot(2,1,1);
    plot(t,x);
    hold on;
    plot(t,y,'--');
    title('Grafico de t por x,y')
	xlabel('Valores de t')
	ylabel('Valores de x,y')
    legend('x(t)', 'y(t)');
    subplot(2,1,2);
    plot(y,x);
    axis square;
    title('Grafico de y por x')
	xlabel('Valores de y')
	ylabel('Valores de x')

    
end
