function exercicio1()
    %Use a fun��o linspace para criar vetores id�nticos aos abaixo criando com a nota��o dois-pontos:
    %Entrada:
    %

    t=linspace(4,6,35);
    x=linspace(-4,2);
   
    fprintf('Vetor 1: ');
    for i = 1:length(t)
        fprintf('%.3f\t',t(i));
    end
    fprintf('\n');
    fprintf('Vetor 2: ');
    for j = 1:length(x)
        fprintf('%.3f\t',x(j));
    end
    fprintf('\n');
end
