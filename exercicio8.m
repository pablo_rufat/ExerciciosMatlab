 function exercicio8(x,a,e)
    %O m�todo babil�nico, um antigo sistema para aproxima��o da raiz quadrada de qualquer
    %n�mero positivo a, pode ser formulado como:
    %*ver equa��o no pdf*
    %Escreva uma fun��o bem estruturada no MATLAB com base na estrutura de la�o while
    %para implementar esse algoritmo. Use identa��o adequada, de modo que a estrutura do
    %c�digo fique clara. Em cada passo, estime o erro em sua aproxima��o como:
    %*ver equa��o no pdf*
    %Repita o la�o at� que  seja menor ou igual a um valor espec�fico e projete seu programa
    %de modo que ele retorne o resultado e o erro. Certifique-se qie ele possa avaliar a raiz
    %quadrada de n�meros iguais ou menores que zero. Para o ultimo caso, exiba o resultado
    %como um n�mero imagin�rio, por exemplo, a raiz quadrada de �4 seria retornada com
    %2i. Teste seu programa avaliando a � 0, 2, 10 para  � 1 � 10�4
    %
    %Entrada:
    %exercicio8(1.5,3,0.001)
    
    i='';
    
    if a < 0
        a = abs(a);
        i='i';
    end
    
    xanterior=x;
    
    xnovo = (xanterior + (a./xanterior))./2;
    
    while abs((xnovo - xanterior)/xnovo) > e
        
        xanterior = xnovo;
        xnovo = (xanterior + (a./xanterior))./2;
        
    end
    
    fprintf("Raiz quadrada: %.6f%c\t\tErro: %.6f\n",xnovo,i,abs((xnovo - xanterior)/xnovo));
    
end
