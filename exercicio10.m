function exercicio10(x,y,r)

    circulo = animatedline('Marker','o');
    set(gca,'XLim',[-6,6],'YLim',[-6,6]);
    grid;
    for th = 0:pi/50:2*pi
        addpoints(circulo,(r * cos(th) + x),(r * sin(th) + y));
        drawnow;
    end

end