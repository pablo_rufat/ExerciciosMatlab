 function exercicio6(P,i)
    %H� formulas econ�micas dispon�veis para calcular pagamentos anuais de empr�stimos.
    %Considere que voc� tenha emprestado uma quantidade de dinheiro P e tenha concordado
    %em pagar em n pagamentos anuais com taxa de juros i. A f�rmula para calcular o
    %pagamento anual A �:
    %*ver formula no pdf*
    %Escreva uma fun��o no MATLAB para calcular A. Teste-a com P = R$100.000,00
    %e uma taxa de juros de 3.3%a.a. Calcule os resultados para n = 1, 2, 3, 4 e 5 e mostre o
    %resultado em um tabela com cabe�alhos e colunas para n e A
    %
    %Entrada:
    %exercicio6(100000,3.3)
    %
    
    n = 1:5;
    A = P*i*(1+i).^n./((1+i).^n-1);
    y = [n;A];
    fprintf('\n\tn\t\tA\n');
    fprintf('----------------------------\n');
    fprintf('\t%d\t\tR$%.2f\n',y); 
    
end
