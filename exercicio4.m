 function exercicio4()
    %A expans�o em s�rie de Maclaurin para o cosseno �:
    %*ver serie no pdf*
    %Use o MATLAB para criar um gr�fico do cosseno com um gr�fico da expans�o em s�rie
    %at� o termo x^8/8!. Utilize a fun��o nativa factorial no calculo da expans�o em s�rie, e para
    %as abscissas utilize o intervalo x = 0 at� 3?/2
    %
    %Entrada:
    %
    
    x = linspace(0,(3.*pi)./2,100);
    y = 0;
    for n=1:8
        y = y + (((-1).^n).*(x.^(2.*n))./factorial(2.*n));
    end
        
    plot(x,y);
end
