
subplot(2,2,1);
a = busca_inc((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),-601,-599);
hold on;
subplot(2,2,2);
a = busca_inc((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),-269,-268);
hold on;
subplot(2,2,3);
a = busca_inc((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),268,269);
hold on;
subplot(2,2,4);
a = busca_inc((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),599,601);
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碆isse玢o 1创创创创创创创创创\n');
bissec((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),-601,-599);
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创碏alsa posi玢o 1创创创创创创创碶n');
falsaPosicao((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),-601,-599,0.001,10);
fprintf('\n\n\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碆isse玢o 2创创创创创创创创创\n');
bissec((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),-269,-268);
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创碏alsa posi玢o 2创创创创创创创碶n');
falsaPosicao((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),-269,-268,0.001,10);
fprintf('\n\n\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碆isse玢o 3创创创创创创创创创\n');
bissec((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),268,269);
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创碏alsa posi玢o 3创创创创创创创碶n');
falsaPosicao((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),268,269,0.001,10);
fprintf('\n\n\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碆isse玢o 4创创创创创创创创创\n');
bissec((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),599,601);
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创碏alsa posi玢o 4创创创创创创创碶n');
falsaPosicao((@(x)(2.5/(120*50000*30000*600))*((-5)*x.^4 + 6*360000*x.^2 - 129600000000)),599,601,0.001,10);
fprintf('\n\n\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碊efle玢o maxima创创创创创创碶n');

fprintf('Def. max. => %.8f\n',((2.5/(120*50000*30000*600))*(-(-268.328186^5)+(2*360000*(-268.328186)^3)-(129600000000*(-268.328186)))));


function xb=busca_inc(func,xmin,xmax,ns)
    %Programa para localizacao de raizes de uma funcao
    %Entrada:
    % func �> nome de uma funcao
    % xmim,xmax �> intervalo de busca
    % ns �> numero de subintervalos
    if nargin<3, error('Sao neccessarios 3 argumentos de entrada'), end
    if nargin<4, ns=100; end
    x=linspace(xmin,xmax,ns);
    f=func(x);
    nb=0;
    xb=[];
    for k=1:length(x)-1
        if sign(f(k))~=sign(f(k+1))
            nb=nb+1;
            xb(nb,1)=x(k);
            xb(nb,2)=x(k+1);
        end
    end
    if isempty(xb)
        disp('Nenhum subitervalo obtido')
        disp('Verifique o intervalo ou aumente ns')
    else
        fplot(func,[xmin xmax])
        ax=gca;
        ax.XAxisLocation='origin';
        ax.YAxisLocation='left';
        ax.Box='off';
        hold on
        for i=1:length(xb)-1
            plot([xb(i,1) xb(i,2)],[0 0],'marker','square')
        end
        xticks(xmin:0.3:xmax);
    end
end

function bissec(func,a,b,tol,maxit)
    %Programa criado por Prof Marcos Figueredo
    %data: 01/02/2018
    %entrada
    %func �> funcao objetivo
    %a,b �> extremos que contenham uma raiz
    %tol �> tolerancia maxima, caso nao seja informado sera tol=0.0001
    %maxit �> maximo de iteracoes, caso nao seja informado sera 50
    %Exemplo:bissec(@(x) x^3磝�2,1,2)

    if nargin<3
        error('Sao necessarios pelo menos 3 argumentos de entrada!'),end
    test=func(a)*func(b);
    if test>0,error('nao existem solucoes no intervalo!'),end
    if nargin<4||isempty(tol),tol=0.0001;end
    if nargin<5||isempty(maxit), maxit=50;end
    i=1;
    x=0;
    fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
    fprintf('%2s%6s%12s%13s%13s\n','N','a','b', 'xr' ,'|er|');
    fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
    while(abs(a-b)>tol)
        if maxit==i,break,end
        if(i==1)
            x_old=a;
        else
            x_old=x;
        end
        x=(a+b)/2;
        er=abs((x_old-x)/x)*100;
        fprintf('%d\t%6.6f\t%6.6f\t%6.6f\t%8.4f%%\t\n',i,a,b,x,er);
        if func(x)*func(a)<0
            b=x;
        else
            a=x;
        end
        i=i+1;
    end
end

function falsaPosicao(func,a,b,tol,maxit)
    %Programa criado por Prof Marcos Figueredo
    %data: 01/02/2018
    %entrada
    %func �> funcao objetivo
    %a,b �> extremos que contenham uma raiz
    %tol �> tolerancia maxima, caso nao seja informado sera tol=0.0001
    %maxit �> maximo de iteracoes, caso nao seja informado sera 50
    %Exemplo:bissec(@(x) x^3磝�2,1,2)

    if nargin<3
        error('Sao necessarios pelo menos 3 argumentos de entrada!'),end
    test=func(a)*func(b);
    if test>0,error('nao existem solucoes no intervalo!'),end
    if nargin<4||isempty(tol),tol=0.0001;end
    if nargin<5||isempty(maxit), maxit=50;end
    i=1;
    x=0.1;
    fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
    fprintf('%2s%6s%12s%13s%13s\n','N','a','b', 'xr' ,'|er|');
    fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
    while(abs(func(x))>tol)
        if maxit==i,break,end
        if(i==1)
            x_old=a;
        else
            x_old=x;
        end
        x=(a*func(b)-b*func(a))/(func(b)-func(a));
        er=abs((x_old-x)/x)*100;
        fprintf('%d\t%6.6f\t%6.6f\t%6.6f\t%8.4f%%\t\n',i,a,b,x,er);
        if func(x)*func(a)<0
            b=x;
        else
            a=x;
        end
        i=i+1;
    end
end