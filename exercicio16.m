function exercicio16()

    y = 1.37^3 - 7*(1.37^2) - 8*1.37 - 0.35;
    
    fprintf('%.3f <==> %.4f\n',y,y);

    y = ((1.37 - 7)*1.37 + 8)*1.37 - 0.35;
    
    fprintf('%.3f <==> %.4f\n',y,y);
    
end