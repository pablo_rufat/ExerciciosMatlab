clc

fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碏un玢o A创创创创创创创创创创\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('f(x) = sqrt(x) - exp(-x)\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碆isse玢o创创创创创创创创创创\n');
bissec((@(x)sqrt(x) - exp(-x)),0.4,0.5,0.001)
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创碏alsa posi玢o创创创创创创创创碶n');
falsaPosicao((@(x)sqrt(x) - exp(-x)),0.4,0.5,0.001)
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碏un玢o B创创创创创创创创创创\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('f(x) = ln(x) - x + 2\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碆isse玢o创创创创创创创创创创\n');
bissec((@(x)log(x) - x + 2),0.1,0.2,0.001)
bissec((@(x)log(x) - x + 2),3.1,3.2,0.001)
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创碏alsa posi玢o创创创创创创创创碶n');
falsaPosicao((@(x)log(x) - x + 2),0.1,0.2,0.001)
falsaPosicao((@(x)log(x) - x + 2),3.1,3.2,0.001)
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碏un玢o C创创创创创创创创创创\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('f(x) = exp(x/2)-x^3\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碆isse玢o创创创创创创创创创创\n');
bissec((@(x)exp(x/2)-x.^3),1,1.5,0.001)
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创碏alsa posi玢o创创创创创创创创碶n');
falsaPosicao((@(x)exp(x/2)-x.^3),1,1.5,0.001)
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碏un玢o D创创创创创创创创创创\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('f(x) = sen(x) - x^2\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碆isse玢o创创创创创创创创创创\n');
bissec((@(x)sin(x) - x.^2),-0.1,0.1,0.001)
bissec((@(x)sin(x) - x.^2),0.8,1,0.001)
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创碏alsa posi玢o创创创创创创创创碶n');
falsaPosicao((@(x)sin(x) - x.^2),-0.5,0.5,0.001)
falsaPosicao((@(x)sin(x) - x.^2),0.8,1,0.001)
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碏un玢o E创创创创创创创创创创\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('f(x) = x/4 - cos(x)\n');
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创创碆isse玢o创创创创创创创创创创\n');
bissec((@(x)x/4 - cos(x)),-3.7,-3.4,0.001)
bissec((@(x)x/4 - cos(x)),-2.2,-1.9,0.001)
bissec((@(x)x/4 - cos(x)),1.1,1.4,0.001)
fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
fprintf('创创创创创创创创碏alsa posi玢o创创创创创创创创碶n');
falsaPosicao((@(x)x/4 - cos(x)),-3.7,-3.4,0.001)
falsaPosicao((@(x)x/4 - cos(x)),-2.2,-1.9,0.001)
falsaPosicao((@(x)x/4 - cos(x)),1.1,1.4,0.001)

function bissec(func,a,b,tol,maxit)
    %Programa criado por Prof Marcos Figueredo
    %data: 01/02/2018
    %entrada
    %func �> funcao objetivo
    %a,b �> extremos que contenham uma raiz
    %tol �> tolerancia maxima, caso nao seja informado sera tol=0.0001
    %maxit �> maximo de iteracoes, caso nao seja informado sera 50
    %Exemplo:bissec(@(x) x^3磝�2,1,2)

    if nargin<3
        error('Sao necessarios pelo menos 3 argumentos de entrada!'),end
    test=func(a)*func(b);
    if test>0,error('nao existem solucoes no intervalo!'),end
    if nargin<4||isempty(tol),tol=0.0001;end
    if nargin<5||isempty(maxit), maxit=50;end
    i=1;
    x=0;
    fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
    fprintf('%2s%6s%12s%13s%13s\n','N','a','b', 'xr' ,'|er|');
    fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
    while(abs(a-b)>tol)
        if maxit==i,break,end
        if(i==1)
            x_old=a;
        else
            x_old=x;
        end
        x=(a+b)/2;
        er=abs((x_old-x)/x)*100;
        fprintf('%d\t%6.6f\t%6.6f\t%6.6f\t%8.4f%%\t\n',i,a,b,x,er);
        if func(x)*func(a)<0
            b=x;
        else
            a=x;
        end
        i=i+1;
    end
    fprintf('Itera珲es: %d\n',i-1);
end

function falsaPosicao(func,a,b,tol,maxit)
    %Programa criado por Prof Marcos Figueredo
    %data: 01/02/2018
    %entrada
    %func �> funcao objetivo
    %a,b �> extremos que contenham uma raiz
    %tol �> tolerancia maxima, caso nao seja informado sera tol=0.0001
    %maxit �> maximo de iteracoes, caso nao seja informado sera 50
    %Exemplo:bissec(@(x) x^3磝�2,1,2)

    if nargin<3
        error('Sao necessarios pelo menos 3 argumentos de entrada!'),end
    test=func(a)*func(b);
    if test>0,error('nao existem solucoes no intervalo!'),end
    if nargin<4||isempty(tol),tol=0.0001;end
    if nargin<5||isempty(maxit), maxit=50;end
    i=1;
    x=0;
    fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
    fprintf('%2s%6s%12s%13s%13s\n','N','a','b', 'xr' ,'|er|');
    fprintf('创创创创创创创创创创创创创创创创创创创创创创创碶n');
    while(abs(func(x))>tol)
        if maxit==i,break,end
        if(i==1)
            x_old=a;
        else
            x_old=x;
        end
        x=(a*func(b)-b*func(a))/(func(b)-func(a));
        er=abs((x_old-x)/x)*100;
        fprintf('%d\t%6.6f\t%6.6f\t%6.6f\t%8.4f%%\t\n',i,a,b,x,er);
        if func(x)*func(a)<0
            b=x;
        else
            a=x;
        end
        i=i+1;
    end
    fprintf('Itera珲es: %d\n',i-1);
end