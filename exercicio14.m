function exercicio14()
    
    v_real = 0.006737947;

    sn=0;
    fprintf('Aproximação A:\n----------------------------------------------------\n');
    fprintf('n\tSn\t\t\t\t\tErro\n');
    
    for n = 0:20
        sn = sn + (((-1)^n)*((5^n)/factorial(n)));
        fprintf('%d\t%.15f\t%.15f\n',n,sn,abs(sn-v_real));
    end
    
    sn=0;
    fprintf('Aproximação B:\n----------------------------------------------------\n');
    fprintf('n\tSn\t\t\t\t\tErro\n');
    
    for n = 0:20
        sn = sn + 1/(((-1)^n)*((5^n)/factorial(n)));
        fprintf('%d\t%.15f\t%.15f\n',n,sn,abs(sn-v_real));
    end
    
end