
clc;
fprintf('1)\n\n');
newtonRap((@(x)(x./2)-tan(x)),0.1,0.01);
fprintf('\n\n\n');
fprintf('\n\n\n');

fprintf('2)\n\n');
newtonRap((@(x)2*cos(x) - (exp(x)/2)),0.8,0.01);
fprintf('\n\n\n');
fprintf('\n\n\n');

fprintf('3)\n\n');
newtonRap((@(x)x.^5 - 6),1.4,0.01);


function xb=busca_inc(func,xmin,xmax,ns)
    %Programa para localizacao de raizes de uma funcao
    %Entrada:
    % func �> nome de uma funcao
    % xmim,xmax �> intervalo de busca
    % ns �> numero de subintervalos
    if nargin<3, error('Sao neccessarios 3 argumentos de entrada'), end
    if nargin<4, ns=100; end
    x=linspace(xmin,xmax,ns);
    f=func(x);
    nb=0;
    xb=[];
    for k=1:length(x)-1
        if sign(f(k))~=sign(f(k+1))
            nb=nb+1;
            xb(nb,1)=x(k);
            xb(nb,2)=x(k+1);
        end
    end
    if isempty(xb)
        disp('Nenhum subitervalo obtido')
        disp('Verifique o intervalo ou aumente ns')
    else
        fplot(func,[xmin xmax])
        ax=gca;
        ax.XAxisLocation='origin';
        ax.YAxisLocation='left';
        ax.Box='off';
        hold on
        for i=1:length(xb)-1
            plot([xb(i,1) xb(i,2)],[0 0],'marker','square')
        end
        xticks(xmin:0.3:xmax);
    end
end

function newtonRap(func,a,tol,maxit)
    %Programa criado por Prof Marcos Figueredo
    %data: 01/02/2018
    %entrada  
    %func -> funcao objetivo
    %a -> chute inicial
    %tol -> tolerancia maxima, caso nao seja informado sera tol=0.0001
    %maxit -> maximo de iteracoes, caso nao seja informado sera 50
    %Exemplo:bissec(@(x) x^3-x-2,1,2)

    if nargin<2
        error('Sao necessarios pelo menos 2 argumentos de entrada!'),end
    if nargin<3||isempty(tol),tol=0.0001;end
    if nargin<4||isempty(maxit), maxit=50;end
    i=1; 
    xr=0.1;
    syms x;
    f1=matlabFunction(diff(func(x)));
    fprintf('\t=====================================\n');
    fprintf('\t\tMetodo de Newton Raphson\n');
    fprintf('\t=====================================\n\n');
    fprintf('-----------------------------------------------\n');
    fprintf('%2s%6s%12s%13s%13s\n','N','xi','x(i+1)','|er|');
    fprintf('\n-----------------------------------------------\n');
    while(abs(func(xr))>tol)
        if maxit==i,break,end
           xr=a-func(a)/f1(a);
           er=abs((xr-a)/xr)*100;
           fprintf('%d\t%6.6f\t%6.6f\t%8.4f%%\t\n',i,a,xr,er);
           a=xr;
           i=i+1;
    end
end

function secantes(func,a,b,tol,maxit)
    %Programa criado por Prof Marcos Figueredo
    %data: 01/02/2018
    %entrada  
    %func -> funcao objetivo
    %a -> chute inicial
    %tol -> tolerancia maxima, caso nao seja informado sera tol=0.0001
    %maxit -> maximo de iteracoes, caso nao seja informado sera 50
    %Exemplo:secantes(@(x) x^3-x-2,1,2)

    if nargin<3
        error('Sao necessarios pelo menos 3 argumentos de entrada!'),end
    if nargin<4||isempty(tol),tol=0.0001;end
    if nargin<5||isempty(maxit), maxit=50;end
    i=1; 
    xr=0.01;
    fprintf('\t=====================================\n');
    fprintf('\t\tMetodo da Secante\n');
    fprintf('\t=====================================\n\n');
    fprintf('-----------------------------------------------\n');
    fprintf('%2s%10s%10s%12s%14s%13s\n','N','x(i-1)','xi','x(i+1)','|er|');
    fprintf('\n-----------------------------------------------\n');
    while(abs(func(xr))>tol)
        if maxit==i,break,end
           xr=a-(func(a)*(a-b))/(func(a)-func(b));
           er=abs((xr-a)/xr)*100;
           fprintf('%d\t%6.6f\t%6.6f\t%6.6f\t%8.4f%%\t\n',i,a,b,xr,er);
           b=a;
           a=xr;
           i=i+1;
    end
end
