function exercicio13(precisao)
    
    format long;

    s=0;
    si=0;
    
    fprintf('De 1 a 10.000\n');
    fprintf('-----------------------------------------\n');
    fprintf('i\t\tSi\t\t\t\t\tS\n');
    fprintf('...\t\t...\t\t\t\t\t...\n');
    
    for i = 1:10000
        
        si=1/(i^4);
        s = s + si;
        
        if i >= 9980
            fprintf('%d\t%.*f\t%.*f\n',i,precisao,si,precisao,s);
        end
    end
    
    fprintf('De 10.000 a 1\n');
    fprintf('-----------------------------------------\n');
    fprintf('i\t\tSi\t\t\t\t\tS\n');
    fprintf('...\t\t...\t\t\t\t\t...\n');
    
    s=0;
    si=0;
    
    for i = 10000:-1:1
        
        si=1/(i^4);
        s = s + si;
        
        if i<+ 20
            fprintf('%d\t%.*f\t%.*f\n',i,precisao,si,precisao,s);
        end
    end
end