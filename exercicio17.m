function exercicio17()
    x = 0.3*pi;
    n = 0;
    an = 1;
    Sn = an;
    E = inf;
        
    fprintf('n\tan\t\t\tSn\t\t\tErro\n');
    
    while E > 0.000000001
        n = n + 1;
        an = ((-1)^n)*(x^(2*n))/factorial(2*n);
        E = abs(an/Sn);
        Sn = Sn + an;
        
        fprintf('%d\t%.8f\t%.8f\t%.8f\n',n,an,Sn,E);
    end
end
