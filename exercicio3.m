 function exercicio3()
    %A fun��o de densidade de probabilidade normal � uma curva em forma de sino que pode
    %ser representada como
    %*ver equa��o no pdf*
    %Use o MATLAB para gerar um gr�fico dessa fun��o de z = �5 ate 5. Identifique as
    %ordenadas como frequ�ncia e as abacisas com z
    %
    %
    %Entrada:
    %

    z = linspace(-5,5,100);
    y = (1./(sqrt(2./pi))).*exp((-1).*((z.^2)./2));
    
    plot(z,y);
    
    title('Densidade de probabilidade');
    ylabel('frequencia');
    xlabel('z');

end
