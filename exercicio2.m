function exercicio2()
    %Considere circuito el�trico simples consistindo de um resistor, um capacitor e um indutor.
    %a carga no capacitor qptq como uma fun��o do tempo pode ser calculada como
    %*ver equa��o no pdf*
    %onde t � o tempo, q0 � a carga inicial, R � a resist�ncia, L � a indut�ncia e C � a
    %capacit�ncia. Use o MATLAB para gerar um gr�fico desa fun��o de t = 0 a t = 0.8 dado
    %que q0 = 10, R = 60, L = 9 e C = 0.00005
    %
    %
    %Entrada:
    %

    q0 = 10;
    R = 60;
    L = 9;
    C = 0.00005;
    t = linspace(0,0.8,100);
    y = q0*(exp((-1).*R.*t./(2.*L))).*cos(sqrt((1./(L.*C))-(((R./(2.*L)).^2).*t)));
    
    plot(t,y);
end
