 function exercicio7(r,L)
    %O volume V de l�quido em um cilindro horizontal oco de raio r e comprimento L �
    %relacionado � profundidade do l�quido h por:
    %*ver equa��o no pdf*
    %Gere valores de x e y para t [0, 100] com ?t = 1/16 e construa gr�ficos de:
    %Desenvolva uma fun��o no MATLAB para criar um gr�fico do volume versus a profundidade
    %e Teste seu programa.
    %
    %Entrada:
    %exercicio7(5,100)
    
    h = linspace(0,2*r);
    
    V = (r^2*acos((r-h)./r)-(r-h).*sqrt(2*r*h-h.^2))*L;
    plot(h, V) 
    title('Grafico do volume de l�quido em um cilindro horizontal oco')
	xlabel('Profundidae')
	ylabel('Volume')
    
end
